import { Component, OnInit, Input, OnDestroy} from '@angular/core';
import { Post } from '../models/post';
import { PostsService } from '../services/posts.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit, OnDestroy {

  title = 'Posts';
  posts: Post[];
  subscription: Subscription;

  constructor(private postsService: PostsService) { }

  ngOnInit() {
    // debugger;
    this.subscription = this.postsService.postsSubject.subscribe(
      (posts: Post[]) => {
        this.posts = posts;
      }
    );
    this.postsService.emitPosts();
  }

  getPosts() {
    this.postsService.postsSubject.next();
  }

  onSavePosts() {
    this.postsService.savePosts();
    this.postsService.emitPosts();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
