import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'blog';

  constructor() {
    const config = {
      apiKey: 'AIzaSyDZDQuSefCaTg0hpVJ5osk0YqmsvH8t7U4',
      authDomain: 'firebackendbase.firebaseapp.com',
      databaseURL: 'https://firebackendbase.firebaseio.com',
      projectId: 'firebackendbase',
      storageBucket: 'firebackendbase.appspot.com',
      messagingSenderId: '1080744528802'
    };
    firebase.initializeApp(config);
  }
}
