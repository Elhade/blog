import { Injectable } from '@angular/core';
import { Post } from '../models/post';
import { Subject } from 'rxjs';
import * as firebase from 'firebase';
import Datasnapshot = firebase.database.DataSnapshot;


@Injectable({
  providedIn: 'root'
})
export class PostsService {

  posts: Post[];
  postsSubject: Subject<Post[]>;

  constructor() {
    this.postsSubject = new Subject<Post[]>();
    this.getPosts();
  }

  emitPosts() {
    this.postsSubject.next(this.posts);
  }

  // create
  createPost(post: Post) {
    this.posts.push(post);
    // console.log(post);
    this.savePosts();
    this.emitPosts();
  }

  // read
  getPosts(): Post[] | void {
    firebase.database().ref('/posts')
    .on('value', (data: Datasnapshot) => {
      this.posts = data.val() ? data.val() : [];
      this.emitPosts();
      // resolve(this.posts);
    });
  //  return this.posts;
  }

  // update
  savePosts(): Promise<string> {
    let i = 0;
    for (const post of this.posts) {
      post.id = i++;
      /* if (!post.createdAt) {
      //   post.createdAt = new Date();
      // }

      // post.dateServer = post.createdAt.getDate();
      // console.log(post);
      */
    }
    return new Promise (
      (resolve, reject) => {
        firebase.database().ref('/posts').set(this.posts).then(
          () => {
                  resolve();
                },
          (error) => reject(error)
          );
      }
    );
  }

  // delete
  removePost(post: Post) {
    this.posts = this.posts.filter((value: Post) => post.id !== value.id);
    this.savePosts();
    this.emitPosts();
  }

}
