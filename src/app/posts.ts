import { Post } from './models/post';

const firstPost: Post = new Post('Mon premier post');
const secondPost: Post = new Post('Mon deuxième post');
// const otherPost: Post = new Post('Encore un post');

export const POSTS: Post[] = [firstPost, secondPost ];
