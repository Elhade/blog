const CONTENT =
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ' +
                'Etiam molestie lorem pretium massa mollis ultrices id in urna.' +
                ' Vivamus semper lectus at nunc vulputate, id aliquet ipsum tincidunt.' +
                ' Proin elementum, ipsum vitae auctor malesuada, ex nisl auctor lacus,' +
                ' at pulvinar magna libero a velit. Donec commodo vestibulum mi sit amet iaculis.' +
                ' Quisque sit amet interdum augue. Suspendisse malesuada finibus est maximus' +
                ' sagittis. Curabitur commodo in lectus eget placerat. Nunc a faucibus augue,' +
                ' sit amet sodales erat. Pellentesque vitae faucibus eros, ac mollis enim.';

export class Post {
  id: number;
  title: string;
  content: string;
  loveIts: number;
  createdAt: Date;
  dateServer: number;

  constructor( title: string = '', content: string = CONTENT, createdAt: Date = new Date(), loveIts: number = 0) {
    this.title = title;
    this.content = content;
    this.loveIts = loveIts;
    this.createdAt = createdAt;
    this.dateServer = this.createdAt.valueOf();
  }
}
