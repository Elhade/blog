import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PostsService } from '../services/posts.service';
import { Router } from '@angular/router';
import { Post } from '../models/post';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.css']
})
export class NewPostComponent implements OnInit {

  postForm: FormGroup;
  title = 'Nouvel article' ;
  constructor( private formBuilder: FormBuilder,
               private postsService: PostsService,
               private router: Router) { }

  ngOnInit() {
    this.onInitForm();
  }

  onInitForm() {
    this.postForm = this.formBuilder.group({
      title: ['', [Validators.required, Validators.minLength(2)]],
      content: ['', Validators.required]
    });
  }

  onSubmitForm() {
    const postFormValue = this.postForm.value;
    const newPost: Post = new Post(postFormValue.title, postFormValue.content);
    this.postsService.createPost(newPost);
    this.router.navigate(['/posts']);
  }

}
